{
  description = "Currently, in learning purpose";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-23.05";
    zig.url = "github:mitchellh/zig-overlay";
    flake-utils.url = "github:numtide/flake-utils";

    # used for shell.nix
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, ... }@ inputs: let
    overlays = [
      (final: prev: {
        zigpkgs = inputs.zig.packages.${prev.system};
      })
    ];
    systems = builtins.attrNames inputs.zig.packages;
    in
      flake-utils.lib.eachSystem systems (
        system: let
          pkgs = import nixpkgs {inherit overlays system;};
        in rec {
          devShells.default = pkgs.mkShell
            {
              nativeBuildInputs = with pkgs; [
                zigpkgs.master
              ];
            };
                 
          # For compatiability with older versions of the `nix` binary
          devShell = self.devShells.${system}.default;
        }
      );
}
